const mongoose = require('mongoose');


// Schema
const Schema = mongoose.Schema;
const BiodataSchema = new Schema({
    nama: String,
    email: String,
    noTlp: String,
    alamat: String,
    agama: String,
    date: {
        type: String,
        default: Date.now()
    }
});

// Model
const Biodata = mongoose.model('Biodata', BiodataSchema);

module.exports =  Biodata;