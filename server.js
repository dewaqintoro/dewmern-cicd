
// Importing Modules
const mongoose = require('mongoose');
const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const morgan = require('morgan');
const config = require('dotenv/config')

// importing files

// Define Global Variables
const app = express();
const log = console.log;
const PORT = process.env.PORT || 12345; // Step 1

const routes = require('./routes/api');
require('dotenv').config();

// Step 2

mongoose.connect( process.env.MONGODB_URI || 'mongodb://localhost/mern_atlas_dua', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

 
mongoose.connection.on('connected', () => {
    console.log('Mongoose is connected!!!!');
});

// Configuration
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/', routes);

// Step 3
if (process.env.NODE_ENV === 'production') {
    app.use(express.static( 'client/build' ));

    app.get('*', (req, res) => {
        res.sendFile(path.join(__dirname, 'client', 'build', 'index.html')); // relative path
    });
}

// HTTP request logger
// app.use(morgan('tiny'));
// app.use('/api', routes);

app.listen(PORT, () => {
    log(`Server is starting at PORT: ${PORT}`);
});