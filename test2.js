
const supertest = require('supertest');
const server = require('./server.js');
const chai = require('chai');

chai.should();

const api = supertest.agent(server);

describe('Add method', () => {
  it('should connect to the Server', (done) => {
    api.post('/save')
      .set('Connetion', 'keep alive')
      .set('Content-Type', 'application/json')

      .end((err, res) => {
        res.status.should.equal(200);
        res.body.result.should.equal(5);
        done();
      });
  });
})