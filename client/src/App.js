import React from 'react';
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';


import './App.css';

class App extends React.Component {

  state = {
    nama: '',
    email: '',
    noTlp: '',
    alamat: '',
    agama: '',
    posts: []
  };

  componentDidMount = () => {
    this.getBlogPost();
  };


  getBlogPost = () => {
    axios.get('/api')
      .then((response) => {
        const data = response.data;
        this.setState({ posts: data });
        console.log('Data has been received!!');
      })
      .catch(() => {
        alert('Error retrieving data!!!');
      });
  }

  handleChange = ({ target }) => {
    const { name, value } = target;
    this.setState({ [name]: value });
  };


  submit = (event) => {
    event.preventDefault();

    const payload = {
      nama: this.state.nama,
      email: this.state.email,
      noTlp: this.state.noTlp,
      alamat: this.state.alamat,
      agama: this.state.agama
    };


    axios({
      url: '/api/save',
      method: 'POST',
      data: payload
    })
      .then(() => {
        console.log('Data has been sent to the server');
        this.resetUserInputs();
        this.getBlogPost();
      })
      .catch(() => {
        console.log('Internal server error');
      });;
  };

  resetUserInputs = () => {
    this.setState({
      nama: '',
      email: '',
      noTlp: '',
      alamat: '',
      agama: ''
    });
  };

  displayBlogPost = (posts) => {

    if (!posts.length) return null;


    return posts.map((post, index) => (
      <div key={index} className="hasil">

        <img className="card-img-top" src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRsEsXXe-PE5Wnb0aGHQ60tJB66g2z6wQ_CAFiWqekdmous0Sn9" />
        <div className="card-body">
          <ul className="list-group list-group-flush">
            <li className="list-group-item">Nama : {post.nama}</li>
            <li className="list-group-item">Email : {post.email}</li>
            <li className="list-group-item">No Telp : {post.noTlp}</li>
            <li className="list-group-item">Agama : {post.agama}</li>
            <li className="list-group-item">alamat : {post.alamat}</li>
        </ul>
        </div>
        <button onClick={this.props.remove}>Hapus</button>
      </div>
    ));
  }; 

  render() {

    console.log('State: ', this.state);

    //JSX
    return(
      <div className="app container">

        <h2>Menyerupai WebSite2</h2>
        <form onSubmit={this.submit}>

        <div className="form-row">
          <div className="form-group col-md-6">
            <input 
              className="form-control"
              type="text"
              name="nama"
              placeholder="Nama"
              value={this.state.nama}
              onChange={this.handleChange}
            />
          </div>

          <div className="form-group col-md-6">
            <input 
              className="form-control"
              type="email"
              name="email"
              placeholder="Email"
              value={this.state.email}
              onChange={this.handleChange}
            />
          </div>
          </div>

          <div className="form-group">
            <input
              className="form-control" 
              type="number"
              name="noTlp"
              placeholder="Nomor telepon"
              value={this.state.noTlp}
              onChange={this.handleChange}
            />
          </div>

          <div className="form-group"> 
            <label>Agama : </label>
            <select value={this.state.agama} className="form-control" name="agama" onChange={this.handleChange}>
              <option value="">Pilih Agama</option>
              <option value="Islam">Islam</option>
              <option value="Kristen">Kristen</option>
              <option value="Katolik">Katolik</option>
              <option value="Hindu">Hindu</option>
              <option value="Buddha">Buddha</option>
              <option value="Kong Hu Cu">Kong Hu Cu</option>
            </select>
          </div>

          <div className="form-group">
            <textarea
              className="form-control"
              placeholder="Alamat"
              name="alamat"
              cols="30"
              rows="10"
              value={this.state.alamat}
              onChange={this.handleChange}
            >
              
            </textarea>
          </div>

          <button className="btn btn-primary">Simpan</button>
        </form>
        <br/>

        <div className="blog-">
          {this.displayBlogPost(this.state.posts)}
        </div>
      </div>
    );
  }
}


export default App;
